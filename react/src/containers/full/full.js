import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import { Container } from 'reactstrap';
import '../../styles/global/full.css';

// IMPORTING ALL COMPONENTS OF THIS PAGE
import Header from '../../components/header/header';
import Sidebar from '../../components/sidebar/sidebar';
import Breadcrumb from '../../components/breadcrumb/breadcrumb';
// VIEWS
import Dashboard from '../../views/dashboard/dashboard';
import PMS from '../../views/pms/pms';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
            <div className="app-body">
                <Sidebar {...this.props} />
                <main className="main">
                    <Container fluid>
                        <Breadcrumb />
                        <Switch>
                            <Route path="/dashboard" name="Dashboard" component={Dashboard} />
                            <Route path="/pms" name="Dashboard" component={PMS} />
                        </Switch>
                    </Container>
                </main>
            </div>
      </div>
    );
  }
}

export default Full;