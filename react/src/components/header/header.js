import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import '../../styles/global/header.css';
import Cookies from "js-cookie";
class Header extends Component {
    constructor(props) {
        super(props);
    }
    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }
    logoutUser() {
        Cookies.remove('loggedIn');
        this.props.history.push("/");
        window.location.reload();

    }
    render() {
        return (
            <header className="top-navheader">
                <Navbar color="purple" expand="md">
                    <Nav className="nav-toggle" navbar>
                        <NavItem>
                            <NavLink href="#" onClick={this.asideToggle}><i className="material-icons">menu</i></NavLink>
                        </NavItem>
                    </Nav>
                    <NavbarBrand href="/">
                        <img src="assets/images/tcz-logo.png" alt="Logo" style={{ height: "40px", width: "80px" }} />
                    </NavbarBrand>
                    <Nav className="ml-auto flex-row" navbar>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav>
                                <img src="assets/images/avatar.png" className="user-avatar" alt="User Avatar"
                                    style={{ height: "35px", width: "35px", padding: 0, }} />
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Profile</DropdownItem>
                                <DropdownItem>Timeline</DropdownItem>
                                <DropdownItem onClick={() => this.logoutUser()}>Logout</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Navbar>
            </header>
        );
    }
}
export default withRouter(Header);