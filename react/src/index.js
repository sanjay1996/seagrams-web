import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/global/index.css';

import Login from './views/pages/login/login';
import Register from './views/pages/register/register';
// import Page404 from './views/pages/page404/page404';
// import Page500 from './views/pages/page500/page500';
import Full from './containers/full/full';
import Cookies from "js-cookie";

ReactDOM.render((
  <HashRouter>
    <Switch>
      <Route exact path="/login" name="Login Page" component={Login} />
      <Route exact path="/register" name="Register Page" component={Register} />
      {/* <Route exact path="/404" name="Page 404" component={Page404}/>
        <Route exact path="/500" name="Page 500" component={Page500}/> */}
      <Route
        path="/"
        name="Home"
        component={!Cookies.get('loggedIn') ? Login : Full} />
    </Switch>
  </HashRouter>
), document.getElementById('root'));