import React, { Component } from 'react';
import axios from "axios";
import { Row, Col, Table, FormGroup, Label, Input, Button } from 'reactstrap';

let file = null;
class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			isReady: false,
			viewDetails: false,
			item: null
		}
	}
	async componentWillMount() {
		var authOptions = {
			method: 'GET',
			url: 'http://159.89.3.128:2000/getData',
			// data: data,
			headers: {
				'Access-Control-Allow-Origin': '*' // to solve cors problem
			}
		};

		await axios(authOptions).then((res) => {
			this.setState({ data: res.data.data, isReady: true });
		});


	}


	handleUploadFile = (event) => {

		file = event.target.files[0]

		const data = new FormData();

		data.append('myfile', file);

		var authOptions = {
			method: 'POST',
			url: 'http://159.89.3.128:2000/uploadData',
			data: data,
			headers: {
				'Access-Control-Allow-Origin': '*', // to solve cors problem
				'Content-Type': 'multipart/form-data'
			}
		};

		axios(authOptions).then((response) => {
			console.log(response);
			if (response.status == 200) {
				window.location.reload();
			} else {
				alert("File Upload Failed.")
			}
		});
	}

	openDeatails(item) {
		this.setState({ item, viewDetails: true });
	}
	closeDetails() {
		this.setState({ viewDetails: false })
	}

	render() {
		if (this.state.isReady == false && this.state.data == []) {
			return (
				<div>
					<h1>Loading</h1>
				</div>
			);
	} else {
			return (
				<Row>
					{this.state.viewDetails == false ?
						<div>
							<Col sm="12">
								<div className="card-box">
									<div className="card-container">
										<Row className="justify-content-between">
											<Col sm="3">
												<FormGroup>
													<Label for="filter">Third Line Manager</Label>
													<Input type="select" name="filter" id="exampleSelect">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
													</Input>
												</FormGroup>
											</Col>
											<Col sm="3">
												<FormGroup>
													<Label for="filter">Second Line Manager</Label>
													<Input type="select" name="filter" id="exampleSelect">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
													</Input>
												</FormGroup>
											</Col>
											<Col sm="3">
												<FormGroup>
													<Label for="filter">First Line Manager</Label>
													<Input type="select" name="filter" id="exampleSelect">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
													</Input>
												</FormGroup>
											</Col>
											<Col sm="3">
												<form>
													<FormGroup>
														<Label for="file">Upload Users</Label>
														<Input type="file" name="myfile" id="exampleFile" onChange={this.handleUploadFile} />
													</FormGroup>
													{/* <Button color="success" type="submit" onClick={() => this.sendFile()}>Submit</Button> */}
												</form>
											</Col>
										</Row>
									</div>
								</div>
							</Col>
							<Col sm="12">
								<div className="card-box">
									<h3 className="card-title">Total Employees</h3>
									<div className="card-container">
										<Table striped hover bordered responsive>
											<thead>
												<tr >
													<th>SL. No.</th>
													<th>Deputy ID</th>
													<th>Name</th>
													<th>HQ.</th>
													<th>State</th>
													<th>Designation</th>
													<th>Reporting Manager</th>
													<th>KPI Score</th>
													<th>Project</th>
													<th>Manager's Score</th>
													<th>Manager's Rating</th>
													<th>Manager's Comments</th>
													<th>Training Needs</th>
													<th>Manager's Comments</th>
												</tr>
											</thead>
											<tbody>
												{this.state.data.map((item, index) => {
													return (
														// <tr key={index} onClick={() => this.openDeatails(item)>
														<tr key={index} >
															<td>{item.SLNo}</td>
															<td>{item.DeputyID}</td>															<td>{item.Name}</td>
															<td>{item.HQ}</td>
															<td>{item.State}</td>
															<td>{item.Designation}</td>
															<td>{item.ReportingManager}</td>
															<td>{item.KPIScore}</td>
															<td>{item.Project}</td>
															<td>{item.ManagersScore}</td>
															<td>{item.ManagersRating}</td>
															<td>{item.ManagersCommentsRating}</td>
															<td>{item.TrainingNeeds}</td>
															<td>{item.ManagersCommentsTraining}</td>
														</tr>
													)
												})}
											</tbody>
										</Table>
									</div>
								</div>
							</Col>
						</div>
						:
						<Col sm="12">
							<div className="card-box">
								<h3 className="card-title style">Details</h3>
								<div className="card-container">
									<Row>
										<div className="col-sm-6">
											<Table striped hover bordered responsive>
												<tbody>
													<tr>
														<td>SL No</td>
														<td>{this.state.item.SLNo}</td>
													</tr>
													<tr>
														<td>Deputy ID</td>
														<td>{this.state.item.DeputyID}</td>
													</tr>
													<tr>
														<td>Name</td>
														<td>{this.state.item.Name}</td>
													</tr>
													<tr>
														<td>HQ.</td>
														<td>{this.state.item.HQ}</td>
													</tr>
													<tr>
														<td>State</td>
														<td>{this.state.item.State}</td>
													</tr>
													<tr>
														<td>Designation</td>
														<td>{this.state.item.Designation}</td>
													</tr>
													<tr>
														<td>Reporting Manager</td>
														<td>{this.state.item.ReportingManager}</td>
													</tr>
												</tbody>
											</Table>
										</div>
										<div className="col-sm-6">
											<Table striped hover bordered responsive >
												<tbody>
													<tr>
														<td>KPI Score</td>
														<td>{this.state.item.KPIScore}</td>
													</tr>
													<tr>
														<td>Project</td>
														<td>{this.state.item.Project}</td>
													</tr>
													<tr>
														<td>Manager's Score</td>
														<td>{this.state.item.ManagersScore}</td>
													</tr>
													<tr>
														<td>Manager's Rating</td>
														<td>{this.state.item.ManagersRating}</td>
													</tr>
													<tr>
														<td>Manager's Comments</td>
														<td>{this.state.item.ManagersCommentsRating}</td>
													</tr>
													<tr>
														<td>Training Needs</td>
														<td>{this.state.item.TrainingNeeds}</td>
													</tr>
													<tr>
														<td>Manager's Comments</td>
														<td>{this.state.item.ManagersCommentsTraining}</td>
													</tr>
												</tbody>
											</Table>
										</div>
									</Row>
								</div>
								<Button onClick={() => this.closeDetails()} style={{ float: "right" }}>CLOSE</Button>
							</div>
						</Col>
					}
				</Row>
			);
		}
	}
}
export default Dashboard;
