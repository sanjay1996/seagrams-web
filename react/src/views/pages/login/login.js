import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Cookies from "js-cookie";
import { withRouter } from "react-router-dom";
import { Alert, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import Axios from "axios";

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: "admin@techchefz.com",
      password: "Password",
      enteredUserName: "",
      enteredPassword: "",
      err: false
    }
  }

  loginUser() {

    // if ((this.state.enteredUserName) === (this.state.username)) {

    //   if (this.state.enteredPassword === this.state.password) {
    //     Cookies.set("loggedIn", "true");
    //     this.props.history.push("/pms");
    //     window.location.reload();
    //   } else {
    //     this.setState({ err: true })
    //   }
    // } else {
    //   this.setState({ err: true })
    // }
    fetch('http://159.89.3.128:2000/login', {
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*', // to solve cors problem
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: this.state.enteredUserName, password: this.state.enteredPassword })
    }).then((res) => res.json()).then((result) => {
      if (result.success == true) {
        Cookies.set("loggedIn", "true");
        this.props.history.push("/pms");
        window.location.reload();
      } else {
        alert(result.message);
      }
    });

  }

  render() {
    return (
      <div className="app-login-register-container">
        <div className="login-logo" style={{
          textAlign: "center",
          width: "100%", justifyContent: "center",
          alignItems: "center", justifyItems: "center"

        }}>
          <img src="assets/images/logo-black.png" className="login-logo" />
        </div>
        <h3>login</h3>
        {this.state.err !== false ? <Alert color="danger">Please Enter Valid Credentials</Alert> : null}
        <Form>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input
              type="email"
              name="email"
              id="email"
              placeholder="Email"
              value={this.state.enteredUserName}
              onChange={(event) => this.setState({ enteredUserName: event.target.value })} />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password"
              name="password"
              id="password"
              placeholder="Password"
              value={this.state.enteredPassword}
              onChange={(event) => this.setState({ enteredPassword: event.target.value })}
            />
          </FormGroup>
          <div className="flex-row align-items-center justify-content-between">
            <Button color="success" onClick={() => this.loginUser()}>Login</Button>
            <Link to="/register" className="pull-right">Create an Account</Link>
          </div>
        </Form>
      </div>
    );
  }
}

export default withRouter(Login);