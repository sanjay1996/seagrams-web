import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import '../../../styles/pages/loginRegister.css';
import Axios from 'axios';
import Cookies from "js-cookie";

class Register extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: null,
            email: null,
            password: null
        };
    };


    onRegisterClick() {
        fetch('http://159.89.3.128:2000/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: this.state.email, name: this.state.name, password: this.state.password })
        }).then((res) => res.json()).then((result) => {
            console.log('====================================');
            console.log(result);
            console.log('====================================');
            Cookies.set("loggedIn", "true");
            this.props.history.push("/pms");
        });
    }

    render() {
        return (
            <div className="app-login-register-container">
                <h3>register</h3>
                <Form>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" id="name" placeholder="Name" onChange={(event) => this.setState({ name: event.target.value })} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="email" name="email" id="email" placeholder="Email" onChange={(event) => this.setState({ email: event.target.value })} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password</Label>
                        <Input type="password" name="email" id="password" placeholder="Password" onChange={(event) => this.setState({ password: event.target.value })} />
                    </FormGroup>
                    <div className="flex-row align-items-center justify-content-between">
                        <Button color="success" onClick={() => { this.onRegisterClick() }}>Register</Button>
                        <Link to="/login" className="pull-right">I am already a member</Link>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(Register);
