import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Row, Col, Table, Badge } from 'reactstrap';

const data = {
	labels: [
		'ASM',
		'TSM',
        'SE'
	],
	datasets: [{
		data: [300, 50, 100],
		backgroundColor: [
		'#FF6384',
		'#36A2EB',
        '#FFCE56'
		],
		hoverBackgroundColor: [
		'#FF6384',
		'#36A2EB',
        '#FFCE56'
		]
	}]
};

class Dashboard extends Component {
    
    render() {
   
    return (
        <Row>
            <Col sm="6">
                <div className="card-box">
                    <h3 className="card-title">Total Employees</h3>
                    <div className="card-container">
                        
                        <Doughnut data={data} />
                    </div>
                </div>
            </Col>
        </Row>
    );
  }
}

export default Dashboard;
